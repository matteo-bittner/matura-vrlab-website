Title: Entwicklung von virtuellen Welten mit Unity
Date: 1970-01-01

Unity ist eine IDE zum Entwickeln von interaktiven 3D-Inhalten.

- [Unity downloaden und installieren](https://store.unity.com/download)

### Entwickeln mit Unity

- [Explore cross-platform mixed reality development on Meta Quest 3](https://unity.com/blog/engine-platform/cross-platform-mixed-reality-development-on-meta-quest-3)
- [Unity Documentation](https://docs.unity.com)
