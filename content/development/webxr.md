Title: Entwicklung von virtuellen Welten mit Web Standards
Date: 1970-01-01

Hintergrundinformationen zu WebXR findet man auf der Website der
<q>Immersive Web Working Group</q>. Auf der unteren Hälfte der Seite
findet man Demos und Links zu JavaScript Frameworks.

- [WebXR](https://immersiveweb.dev) (Immersive Web Working Group)

### Frameworks (Auswahl)

- [A-Frame](https://aframe.io/docs/1.6.0/introduction/)
- [three.js](https://threejs.org/examples) (examples)
