Title: VR-Inhalte von Microsoft
Date: 1970-01-01

Microsoft bietet keine VR-Inhalte selbst an, aber der App Store ist voll
von Windows Apps diverser Anbieter.

- [Apps von VictoryXR](https://apps.microsoft.com/search/publisher?name=VictoryXR&hl=de-ch&gl=CH)

### Suchanfragen (Microsoft App Store)

- [Suche: Anatomie Apps](https://apps.microsoft.com/search?query=anatomy&hl=de-ch&gl=CH&department=Apps)
- [Suche: VR Apps](https://apps.microsoft.com/search?query=vr&hl=de-ch&gl=CH&department=Apps)
