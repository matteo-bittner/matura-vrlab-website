Title: VR-Inhalte von Betzold
Date: 1970-01-01

Die Betzold Lernmedien GmbH aus Schaffhausen vertreibt für Schulen das
ClassVR von Eduverse.

- [ClassVR auf Betzold](https://www.betzold.ch/search/?q=ClassVR)
- [ClassVR Website](https://www.classvr.com)
- [Eduverse](https://eduverse.com) > [Avantis World](https://www.avantisworld.com/)
