Title: VR-Inhalte von Meta / Facebook
Date: 1970-01-01

Meta hat in seinem Store viele interessante Inhalte.

- [Search: anatomy](https://www.meta.com/en-gb/experiences/search/?q=anatomy)
- [Search: history](https://www.meta.com/en-gb/experiences/search?q=history)
- [Anne Frank](https://www.meta.com/en-gb/experiences/1958100334295482)
- [Ocean Rift](https://www.meta.com/en-gb/experiences/2134272053250863/) (Unterwasserwelt)

Ausserdem findet man viele weitere Inhalte im Web.
