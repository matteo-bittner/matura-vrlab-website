Title: VR-Inhalte von Apple
Date: 1970-01-01

Apple bietet selbst keine VR-Unterrichtsinhalte an, es gibt aber
Drittanbieter, die Inhalte über den App Store bereitstellen.

- [Education Apps für Apple Vision Pro](https://apps.apple.com/us/vision/editorial/6503710138)
