Title: Aufgaben im VR-Lab
Date: 1970-01-01

1. **Wartung der Hardware und Software** (Aktualisierungen, Kauf, Miete,
   Leasing, Aushandeln und Betreuen von Wartungsverträgen)
1. **Wartung der Bibliothek von Lerninhalten** (Überprüfung, Aktualisierung,
   Weiterentwicklung)
1. **Testen und Auswahl von Lerninhalten** (bei Ankauf und bei Recherche von
   freien Inhalten)
1. **Entwicklung von Lerninhalten** (Eigenentwicklung, Betreuung von Agenturen
   bei externer Beauftragung)
1. **Koordination mit anderen Schulen** (Ideen holen, entwickelte Inhalte
   teilen, Entwicklungs- oder Anschaffungskosten teilen, Crowdfunding)

## Siehe auch

  - [Team]({filename}team.md)
