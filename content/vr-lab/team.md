Title: Die virtuellen Gestalter
Date: 1970-01-01

Das VR-Lab wird aktuell betreut von Schüler*innen und Lehrpersonen unserer Schule.

### Teilnehmer

- Matteo Bittner, 25Mc

### Betreuer

- Marcello Indino, Rektor

## Siehe auch

- [Aufgaben]({filename}aufgaben.md)
