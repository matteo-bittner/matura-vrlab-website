Title: Definition
Date: 1970-01-01

Wenn man die Definition mithilfe einer Suchmaschine nachschlägt, kommt man
in vielen Fällen, auf folgendes Ergebnis: <q>Die virtuelle Realität ist
eine computergenerierte Wirklichkeit mit Bild (3D) und in vielen Fällen auch
Ton</q>. Jedoch, hatte die Idee von virtueller Realität schon in früherer
Zeit, in welcher Computer noch nicht existiert hatten, verschiedene
Personen inspiriert, welche infolge analoge Ideen entworfen und kreiert haben.
