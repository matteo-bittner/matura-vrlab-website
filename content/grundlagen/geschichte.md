Title: Geschichte und Entwicklung
Date: 1970-01-01

Man könnte sagen, dass das Interesse für eine virtuelle Realität sehr
früh in Form einer immersiven Unterhaltung entstand, und zwar bereits
im 16.~Jahrhundert. Es braucht allerdings bis zum Jahr 1962, bis wir
ein erstes digitales Exemplar zu sehen bekommen, das Sensorama, ein
4D-Kino. Schauen wir uns die wichtigsten Meilensteine der Entwicklung
von VR näher an.

## Die analoge virtuelle Realität

### Trompe l'oeil

Das Trompe l'oeil, ist eine Art von Gemälden, welche in 3d gezeichnet werden.
Ihre Besonderheit ist, dass das Bild real aussieht.

Zum Beispiel wie in folgender
[Abbildung](https://gitlab.com/matteo-bittner/matura-virtual-reality-unterricht/-/blob/main/images/violinen-illusion.jpg?ref_type=heads)
denkt man das hinter der Tür sich noch eine Tür befindet, an welcher eine Violine
festgemacht wurde. Diese Violine hat die Schatten, welche auch bei einer realen Violine
mit dieser Beleuchtung, vorhanden wären. Auf den ersten Blick scheint sie griffbereit
dort zu hängen, doch wenn man sie genauer betrachtet merkt man, dass es nur ein Bild ist.
Dieses Kunstwerk findet man im Chatsworht House in Derbyshire(England).

Ein anderes Beispiel wäre diese
[Abbildung](https://gitlab.com/matteo-bittner/matura-virtual-reality-unterricht/-/blob/main/images/leicester-square-panorama.jpg?ref_type=heads).
Hier ist es zwar kein
Objekt, welches griffbereit scheint, sondern ein ganzes Panoramabild, das den Eindruck
vermittelt, an einem anderen Ort zu sein. Dies ist ein Beispiel für immersive Unterhaltung
des 18. und 19. Jahrhunderts, vergleichbar mit dem, was wir heute durch Head-Mounted
Displays (HMDs) erleben können. Solche Kunstwerk  waren auch Inspiration
für nachfolgende Erfinder. Dieses Panorama, welches im Jahre 1801 von Robert
Mitchell vollendet wurde, befindet sich im Leicester Square in London.

### Das Stereograph

Mit diesem Objekt, namens Stereograph, kommen wir der virtuellen Realität schon näher.
Der Stereograph entstand kurz nach der Erfindung der Fotografie. Wie man in der Abbildung
sehen kann, besteht der Stereograph aus einer Brille und einem Brett, auf welches man
zwei fast identische Bilder, die von zwei leicht verschobenen Sichtpunkten gemacht
wurden, hinstellt. In vielen Fällen sind die Bilder auch noch gekrümmt.
Sobald man die Distanz, zwischen der Brille und den Bildern, richtig
eingestellt hat und durch die Brille schaut, erscheint ein 3D Bild. Dieses Gerät
wurde schnell sehr populär, denn im 20. Jahrhundert waren Reisen keine Selbstverständlichkeit
für jedermann, dies lag am einfachen Grund, das reisen sehr teuer waren.
Mit dieser Erfindung konnten die ärmeren Leute die berühmtesten architektonischen
Kunstwerke in 3D-Form erblicken. Nach und nach wurden Stereographen auch in
Bibliotheken zur Verfügung gestellt.

### Link-Trainer

Edward Link schuf 1929 den Link-Trainer, die erste Flugsimulationsmaschine zur
Pilotenausbildung. Ursprünglich als Unterhaltungsgerät auf Jahrmärkten und in
Spielhallen vermarktet, betrachtete das Militär diese Maschine anfänglich
lediglich als Spielzeug und lehnte ihren Einsatz in der Pilotenausbildung ab.
Seit dem Zweiten Weltkrieg sind Flugsimulatoren integraler Bestandteil der
virtuellen Realität. Verschiedene Unternehmen, darunter Evans \& Sutherland,
haben sich auf die Entwicklung von Flugsimulatoren spezialisiert.

## Virtuelle Realität in der virtuellen Zeit

### Sensorama Simulator

Das Sensorama repräsentiert eines der frühesten Beispiele für immersive,
multisensorische Technologie. Diese wegweisende Technologie wurde 1962 von
Morton Heilig entwickelt und zählt zu den ersten Virtual-Reality-Systemen (VR).
Der Sensorama-Simulator bot eine vielfältige Palette von Effekten. Nutzer
tauchten in das System ein, indem sie ihren Kopf vor einen Bildschirm hielten,
auf dem ein Stereo-Weitwinkelfilm abgespielt wurde. Zusätzlich zu visuellen
Reizen erhielten die Nutzer auch 3D-Klänge sowie Wind- und Geruchsimpressionen.
Darüber hinaus erstellte das Gerät leichte haptische Bewegungen. Diese Innovation
demonstriert eindrucksvoll die Spitzenleistungen analoger Technologien, die
Filmemachern und Kreativen zur Verfügung standen.

### Sword of Damocles System

Das Sword of Damocles System wurde im Jahr 1968 entwickelt und wird von vielen
als das erste digitale Virtual-Reality-System angesehen. Es umfasst ein
kopfmontiertes Display, das an einem mechanischen Gestell von der Decke herabhängt.
Obwohl es bereits über die grundlegenden Funktionen verfügte, die heute mit
Virtual-Reality-Systemen verbunden sind, war es technisch noch sehr rudimentär.
Das kopfmontierte Display verfolgte die Bewegungen des Nutzers und passte die
Grafiken entsprechend an, um eine Ich-Perspektive zu simulieren. Trotz der damals
noch begrenzten grafischen Darstellung mit Y-Frame-Grafiken und niedriger Auflösung,
legte das System den Grundstein für spätere Entwicklungen von Head-Mount-Displays.

### VIEW(Virtual Interface Environment Workstation)

Das System VIEW Workstation der NASA wurde in den 1980er Jahren entwickelt und
stellt gewissermassen einen Vorläufer heutiger Virtual-Reality-Systeme dar. Es
handelt sich um ein tragbares stereoskopisches Anzeigesystem, das es dem Benutzer
 ermöglicht, in eine künstlich generierte Computerumgebung oder eine reale
 Umgebung einzutauchen, die durch entfernte Videokameras übertragen wurde.
 Der Benutzer ist in der Lage, sich frei zu bewegen und trägt Kopfhörer sowie
 Handschuhe, die eine präzise Erfassung seiner Fingerbewegungen ermöglichen.
 Zudem kann das System die Blickrichtung des Benutzers verfolgen. Die NASA nutzte
 dieses System für diverse Projekte, darunter die Schulung von Astronauten.
 In den 1990er Jahren markierte diese Technologie gewissermassen den Beginn
 einer neuen Industrie, bevor Teile davon von der Firma VPL übernommen wurden.

### Virtual research flight helmet

Der Virtual Research Flight Helmet ist ein Virtual-Reality-Headset, das von
Unternehmen Virtual Research hergestellt wurde. Es wurde erstmals im August 1991
eingeführt und markierte das Debüt des Unternehmens im Bereich der VR-Headsets.
Der Virtual Research Flight Helmet wurde speziell für den Einsatz in
Avionik-Simulationen konzipiert.
Dieses Headset erforderte eine Verbindung zu einem PC, um genutzt werden zu
können, nutzte ein nicht positionsabhängiges 3D-Tracking und wies eine
vergleichsweise niedrige Auflösung pro Auge von 360x240 auf.
Ein zentrales Problem bestand darin, dass die Systeme erhebliche Rechenleistung
erforderten, die zu jener Zeit, insbesondere bei Heimcomputern, nicht ausreichend
vorhanden war. 3D-Grafikbeschleuniger waren damals nur selten verfügbar.
Zusätzlich wogen die Headsets 1670 g und waren kostenintensiv.
In den folgenden zehn Jahren sanken die Kosten für derartige Systeme, während
gleichzeitig Bestrebungen unternommen wurden, auch im Bereich der
Unterhaltungselektronik Fuss zu fassen.

### CAVE

CAVE ist ein Akronym, das für Cave Automatic Virtual Environment steht. Die erste
CAVE wurde von Carolina Cruz-Neira, Daniel Sandin und Thomas De Fanti an der
Universität Illinois entwickelt.
Für eine präzise Manipulation von Objekten ist eine klare Vorstellung von ihrer
Position und Handhabung erforderlich. Sowohl CAVE-Systeme als auch Head-Mounted
Displays vermitteln ein realistisches Gefühl von Nähe und räumlicher Tiefe der
betrachteten Objekte. Bei entsprechendem Budget besteht die Möglichkeit, ein
hochwertiges Head-Mounted Display zu erwerben. Verschiedene Unternehmen
produzierten kopfgetragene Displays, um die Bedürfnisse der Industrie und des
akademischen Sektors nach hochwertigen Systemen zu erfüllen.
Im Gegensatz zu einem Head-Mount Display, bei dem die Bilder direkt auf dem Kopf
getragen werden, sind bei einem CAVE-ähnlichen System die Bilder um den Betrachter
herum an den Wänden positioniert. Dies erfordert eine fortlaufende Aktualisierung
der Bilder an den Wänden durch den Computer. Ein Head-Tracker bleibt dennoch
unerlässlich. Ein wesentlicher Vorteil dieser Systeme liegt in der minimalen
Latenzzeit, sodass Bewegungen des Kopfes nahezu verzögerungsfrei umgesetzt werden.
Eine typische CAVE besteht aus drei Wänden und einem Boden, wobei jeder Wand ein
eigener Computer zugeordnet werden kann. Durch die Verwendung mehrerer Computer
lässt sich die Grafikleistung und -qualität steigern. CAVEs sind in Laboren, die
sich mit virtueller Realität beschäftigen, sowie in spezifischen
ingenieurwissenschaftlichen Disziplinen, wie beispielsweise bei Jaguar Land Rover
im Vereinigten Königreich, weit verbreitet. Dort werden CAVE-ähnliche Systeme
genutzt, um verschiedene Aspekte von Fahrzeugen zu analysieren, beispielsweise
die Benutzerinteraktion mit dem Auto, einschliesslich Lenkung sowie Be- und
Entladen des Kofferraums. Diese Technologie bietet einen klaren Vorteil bei der
Bewältigung komplexer, dreidimensionaler Aufgaben.
Für eine präzise Manipulation von Objekten ist eine klare Vorstellung von ihrer
Position und Handhabung erforderlich. Sowohl CAVE-Systeme als auch HMD
vermitteln ein realistisches Gefühl von Nähe und räumlicher Tiefe der
betrachteten Objekte. Bei entsprechendem Budget besteht die Möglichkeit, ein
hochwertiges HMD zu erwerben. Verschiedene Unternehmen
produzierten kopfgetragene Displays, um die Bedürfnisse der Industrie und des
akademischen Sektors nach hochwertigen Systemen zu erfüllen.

### Sony

Schon im Jahre 1997 hat Sony ihr erstes HMD entwickelt, welches <q>Glasstron</q>
genannt wurde. 2002 erschien das <q>PUD-J5A</q>, aber es war nur im japanischen Sonystore
verfügbar. Für den Weltmarkt konzipierte Sony, im Jahre 2016, ein VR-Headset, welches
in Kombination mit der PS4-Konsole, welche schon sehr verbreitet war, zum Funktionieren
gebracht werden konnte. Dieses Produkt veränderte bei einigen Gamern die Spielweise.

### Oculus/Meta

Oculus, eine Firma gegründet von Palmer Luckey und 2 Jahre später gekauft von Facebook,
ist mit Sony ein Marktführer in der Kategorie der virtuellen Realität. Sie materialisierten
ihr erstes Massenproduktions-Konzept im Jahre 2013, dessen Sinn es war verfügbar für alle
zu sein, indem es zu einem leistbaren Preis verkauft wurde, und trotzdem sehr gute Qualität
behielt. Der Name des Produktes ist <q>Oculus Rift</q>. Später brachten sie weitere bessere
Modelle, zum Beispiel <q>Oculus Rift S</q> oder die modernste Gruppe <q>Oculus Quest</q>.

Die heutige von Facebook gekaufte Oculus heisst *Meta Quest*. Die *Meta Quest 3*
ist das neuste Produkt, welches Meta bis jetzt vorgestellt hat. Beim Kauf, bekommt man
gewöhnlicherweise zwei Controller, ein Steuerungsgerät für jede Hand, mitgeliefert.
Diese müssen aber nicht immer verwendet werden,
denn man kann auch beide Hände als Maus verwenden. Die Einrichtung des Geräts ist ziemlich
intuitiv und einfach, vor allem wenn man das einen Jugendlichen überlässt. Sobald man die
Quest eingerichtet hat, wird es sofort zu einem AR-Erlebnis. Im Sichtfeld des
Nutzers wird eine Appleiste visualisiert mit einem Browser, Appstore und paar andere
nützliche Applikationen. Die weiteren Angebote kann der User sich entweder im virtuellen
Appstore herunterladen, oder mit dem Mobilgeräte einfach über die Meta Quest mobile App.
Dieses Erlebnis hatte der Autor dieser Arbeit bei seiner Nutzung der *Meta Quest 3*.

### Apple Vision Pro

Virtual Reality (VR) in einem einzigen, fortschrittlichen Gerät vereint.
Es wurde im Juni 2023 vorgestellt und markiert Apples ersten bedeutenden
Vorstoss in den Bereich des räumlichen Computings. Dieses MR-Gerät
ist darauf ausgelegt, digitale Inhalte mit der realen Welt zu
verschmelzen und bietet eine breite Palette an Funktionen, die von
persönlicher Produktivität über Unterhaltung bis hin zu Kommunikation und
professionellen Anwendungen reichen.

Ausschlaggebend sind, wie typisch bei Apple, die Einfachheit der Bedienung
(z.B. die Benutzung der Augen als Maus), die realistische Darstellung
(z.B. die virtuellen Monitore wirken wie echt und unbeweglich im Raum
platziert, die Objekte haben sogar einen Schatten) und die nahtlose
Integration mit anderen Geräten. Damit ist es möglich, durch die Brille
alle Monitore in den Büros zu ersetzen bzw. die Fähigkeiten von Macbooks
nützlich zu erweitern.
