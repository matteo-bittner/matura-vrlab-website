Title: VR, AR, MR, XR
Date: 1970-01-01

Wir unterscheiden mehrere Akronyme und Begriffe im Thema <q>Virtuelle Realität</q>.

- VR
- AR
- MR
- XR

### Virtual Reality (VR)

Virtuelle Realität kann als computergenerierte virtuelle Umgebung
beschrieben werden, die darauf abzielt, die reale Umgebung des Benutzers
zu ersetzen. In diesem Fall existiert nur noch die virtuelle Sichtweise
und nicht mehr die reale Welt. Die Realität in der virtuellen Realität
ist daher nicht die tatsächliche Welt, sondern eine synthetische,
computergenerierte Welt.

Die virtuelle Realität wird durch drei Hauptmerkmale charakterisiert.
Erstens betont die virtuelle Realität Autonomie und Handlungsfähigkeit,
wobei der Benutzer stets die Kontrolle behält. Dies wird durch
Head-Tracking und die Verwendung des Körpers als Eingabegerät erreicht.
Durch das Tragen eines Headsets wird das Gerät im 3D-Raum verfolgt,
während der Körper als Eingabegerät genutzt werden kann. Dies geschieht
oft mithilfe von Virtual-Reality-Controllern. Es wird jedoch zunehmend
beliebter, die Hände als Eingabegeräte zu verwenden. Neben den Händen
kann auch die Sprache als Eingabegerät dienen.

Dies führt uns zum zweiten Hauptmerkmal, nämlich dass die virtuelle
Realität eine natürliche Interaktion ermöglicht, bei der Gesten mit dem
Controller, den Händen oder den Fingern sowie die Sprache als
Eingabemöglichkeiten genutzt werden.

Das dritte Merkmal der virtuellen Realität ist, dass man ein Gefühl der
Präsenz vermittelt, ein Gefühl des <q>Dabeiseins</q>. Wenn man also
in die virtuelle Welt eintaucht, erlebt man ein intensives Gefühl der
Anwesenheit. Dieses Eintauchen wird durch die Stimulation mehrerer
unserer Sinne erreicht.

### Augmented Reality (AR)

Augmented Reality erweitert die Realität. Der Nutzer sieht also sowohl die
reale Welt als auch die virtuelle Welt. Sie werden quasi miteinander
verschmolzen. Die reale und die virtuelle Welt kann man als
<q>zusammengesetzte Ansicht</q> definieren. Das Konzept der
<q>Reality</q> in AR bezieht sich auf die uns umgebende reale Welt.

Augmented Reality wird durch drei Hauptmerkmale definiert. Erstens
kombiniert Augmented Reality reale und virtuelle Objekte, um eine
gemeinsame Ansicht zu schaffen. Diese Ansicht umfasst nicht nur visuelle
Elemente, sondern kann auch Audioinformationen einbeziehen. Beim Tragen
einer Augmented-Reality-Brille bleibt die reale Welt sichtbar, während
virtuelle Inhalte über sie projiziert werden. Das zweite Hauptmerkmal von
Augmented Reality besteht darin, dass sie in Echtzeit interaktiv ist, was
sowohl explizite als auch implizite Interaktion ermöglicht. Explizite
Interaktion beinhaltet Gesten und Sprache, während implizite Interaktion
durch Bewegungen des Geräts erfolgt, um virtuelle Inhalte nahtlos in die
reale Welt zu integrieren. Das dritte Merkmal von Augmented Reality
besteht darin, dass sie in 3D erfasst wird, um reale und virtuelle Objekte
miteinander zu verbinden und sie in der physischen Welt als
zusammengehörig erscheinen zu lassen. Zusammengefasst sind dies die drei
Hauptmerkmale von Augmented Reality: die Überlagerung virtueller Inhalte
über die reale Welt, wodurch die reale Welt weiterhin sichtbar bleibt und
gleichzeitig virtuelle Elemente hinzugefügt werden.

### Augmented Virtual Reality

Augmented Virtual Reality stellt eigentlich das Gegenteil von AR dar, da
es hauptsächlich virtuell ist und das Virtuelle mit physischen Inhalten
erweitert. In VR sind beispielsweise die physischen Hände nicht sichtbar,
stattdessen sieht man die Controller, die eine gute Simulation der
Handbewegungen und ihrer Position im 3D-Raum bieten. Dies kann als eine
Form erweiterter Virtualität betrachtet werden, bei der reale
Weltkoordinaten integriert werden, einschliesslich der Pose und Bewegung
der Hände in die virtuelle Umgebung.

### Mixed Reality (MR)

Mixed Reality ist ein Begriff, der oft für Verwirrung sorgt. Tatsächlich
wird dies oft anders definiert. Wir definieren gemischte Realität als die
Verschmelzung der physischen und der virtuellen Welt mit unterschiedlichen
Graden der Augmentation.

### Extended Reality (XR)

XR (Extended Reality) ist ein Begriff, der häufig verwendet wird, wobei X
nur ein Platzhalter für Augmented, Virtual oder Mixed Reality ist.
