Project Website
===============

Built with [Pelican](https://docs.getpelican.com).

Prerequisites
-------------

Install Python and Python virtual environment support, e.g.

```console
sudo apt-get install python3-venv
```

Prepare a Python virtual environment:

```console
python3 -m venv venv
source venv/bin/activate
```

Install requirements for Pelican:

```console
python3 -m pip install -r requirements.txt
```

Development
-----------

Generate website using Pelican:

```console
source venv/bin/activate
pelican -r -l
```

Now, look into the `public/` folder to see the generated website.

Alternatively, you can run the website locally using a local webserver, e.g.

```console
cd public
python3 -m http.server
```
