SITENAME = 'Virtual Reality Lab - KSK'
SITEURL = 'https://matteo-bittner.gitlab.io/matura-vrlab-website'
EDITURL = 'https://gitlab.com/matteo-bittner/matura-vrlab-website'

TIMEZONE = 'Europe/Zurich'
DEFAULT_LANG = 'de'
THEME = 'theme'

PATH = 'content'
STATIC_PATHS = []
INDEX_TITLE = ''
INDEX_SAVE_AS = ''
CATEGORIES_SAVE_AS = 'index.html'
ARTICLE_PATHS = [
    'anbieter',
    'development',
    'grundlagen',
    'inhalte',
    'vr-lab',
]
ARTICLE_URL = ARTICLE_SAVE_AS = '{category}/{slug}.html'
AUTHOR_URL = AUTHOR_SAVE_AS = ''
CATEGORY_URL = CATEGORY_SAVE_AS = '{slug}/index.html'
TAG_URL = TAG_SAVE_AS = ''
PAGE_PATHS = ['']
PAGE_URL = PAGE_SAVE_AS = '{slug}.html'

OUTPUT_PATH = 'public'
DIRECT_TEMPLATES = ['index', 'categories']
DELETE_OUTPUT_DIRECTORY = True

MENUITEMS = [
    ('Home', SITEURL + '/'),
    ('Inhalte', SITEURL + '/inhalte'),
    ('Diese Seite bearbeiten', EDITURL),
]

DEFAULT_PAGINATION = False
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
RELATIVE_URLS = True

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
